<?php

namespace App\Http\Controllers;

use App\Http\Resources\GradeResource;
use App\User;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use IvoPetkov\HTML5DOMDocument;

class PercentageCalculator extends Controller
{
    public function index()
    {
        return view('front-end.index');
    }

    public function show($id)

    {

        $existInCourse1 = in_array($id, User::courses1[0]);
        $existInCourse2 = in_array($id, User::courses2[0]);
        if (! $existInCourse1 && ! $existInCourse2) {
            abort(404);
        }
        $type = 1;
        if ($existInCourse2) {
            $type = 2;
        }
        $program = $id;
        return view('front-end.index', compact('program', 'type'));
    }

    public function calculate(Request $request)
    {
        $client = new Client();
        $error = null;
        $url = "https://gradecard.ignou.ac.in/gradecardM/Result.asp";
        if ($request->type === '2') {
          $url=  "https://gradecard.ignou.ac.in/gradecardB/Result.asp";
        }
        $request = $client->post($url,
            array(
                'form_params' => [
                    'Program' => $request->program,
                    'eno' => $request->eno,
                    'submit' => 'Submit',
                    'hidden_submit' => 'OK'
                ]));
        $response = $request->getBody()->getContents();
        if (strpos($response, 'Enrolment Number Not found....')) {
            $error = 'Enrolment Number Not found....';
            $myArray = null;
            $data['error'] = $error;
            return GradeResource::make($data);
        }
        $finalMarks = 0;
        $count = 0;
        $lab = 0;
        $aDataTableHeaderHTML = array();
        $DOM = new HTML5DOMDocument();
        $DOM->loadHTML($response);
        $Header = $DOM->getElementsByTagName('td');
        foreach ($Header as $NodeHeader) {
            array_push($aDataTableHeaderHTML, trim($NodeHeader->textContent));
        }
        $l = 0;
        for ($k = 8; $k <= count($aDataTableHeaderHTML) - 8;) {
            $myArray[$l]['course'] = $aDataTableHeaderHTML[$k];
            $myArray[$l]['assignment'] = $aDataTableHeaderHTML[$k + 1];
            $myArray[$l]['assignmentp'] = ((integer) $aDataTableHeaderHTML[$k + 1] * 25) / 100;
            $k++;
            $myArray[$l]['lab1'] = $aDataTableHeaderHTML[$k + 1];
            if ($aDataTableHeaderHTML[$k + 1] !== '#' && $aDataTableHeaderHTML[$k + 1] !== '-') {
                $lab = $lab + (integer) $aDataTableHeaderHTML[$k + 1];
                $count++;
            }
            $k++;
            $myArray[$l]['lab2'] = $aDataTableHeaderHTML[$k + 1];
            if ($aDataTableHeaderHTML[$k + 1] !== '#' && $aDataTableHeaderHTML[$k + 1] !== '-') {
                $lab = $lab + (integer) $aDataTableHeaderHTML[$k + 1];
                $count++;
            }
            $k++;
            $myArray[$l]['lab3'] = $aDataTableHeaderHTML[$k + 1];
            if ($aDataTableHeaderHTML[$k + 1] !== '#' && $aDataTableHeaderHTML[$k + 1] !== '-') {
                $lab = $lab + (integer) $aDataTableHeaderHTML[$k + 1];
                $count++;
            }
            $k++;
            $myArray[$l]['lab4'] = $aDataTableHeaderHTML[$k + 1];
            if ($aDataTableHeaderHTML[$k + 1] !== '#' && $aDataTableHeaderHTML[$k + 1] !== '-') {
                $lab = $lab + (integer) $aDataTableHeaderHTML[$k + 1];
                $count++;
            }
            $k++;
            $myArray[$l]['marks'] = 0;
            $myArray[$l]['theory'] = $aDataTableHeaderHTML[$k + 1];
            if ($aDataTableHeaderHTML[$k + 1] !== '#' && $aDataTableHeaderHTML[$k + 1] !== '-') {
                $myArray[$l]['marks'] = ((integer) $aDataTableHeaderHTML[$k + 1] * 75) / 100;
            }
            if ($count !== 0) {
                $myArray[$l]['marks'] = (($lab / $count) * 75) / 100;
            }
            $myArray[$l]['total'] = $myArray[$l]['marks'] + $myArray[$l]['assignmentp'];
            $k++;
            $myArray[$l]['status'] = $aDataTableHeaderHTML[$k + 1];
            $k++;
            $k++;
            $finalMarks = $finalMarks + $myArray[$l]['total'];
            $count = 0;
            $lab = 0;
            $l++;
        }
        $count = count($myArray);
        $percentage = ($finalMarks * 100) / ($count * 100);
        $percentage = round($percentage, 2);
        $data = [];
        $data['myArray'] = $myArray;
        $data['error'] = $error;
        $data['percentage'] = $percentage;
        $data['finalMarks'] = $finalMarks;
        return GradeResource::make($data);
    }
}
