@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <google-map class="mb-3"></google-map>
                <contact-form subject="Examaholic customer concern"></contact-form>
            </div>
        </div>
    </div>

@endsection
