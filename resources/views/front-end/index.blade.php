@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">

                <grade-form program="{{$program}}" type="{{$type}}"></grade-form>

            </div>
        </div>
    </div>

@endsection
