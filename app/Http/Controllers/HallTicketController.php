<?php

namespace App\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class HallTicketController extends Controller
{
    public function show(){
        return view('hallticket');
    }

    public function calculate(Request $request)
    {
        $client = new Client();
        $error = null;
        $url = "https://ignouhall.ignou.ac.in/HallTickets/HALL1219/Hall1219.asp";
        $request = $client->post($url,
            array(
                'form_params' => [
                    'Program' => $request->program,
                    'eno' => $request->eno,
                    'submit' => 'Submit',
                    'hidden_submit' => 'OK'
                ]));
        $response = $request->getBody()->getContents();
return $response;
    }
}
