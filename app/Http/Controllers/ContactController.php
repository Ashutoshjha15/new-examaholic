<?php

namespace App\Http\Controllers;


use App\Mail\ContactUsMail;
use App\Notifications\ContactNotification;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    public function store(Request $request)
    {
        Mail::to($request->email)->send(new ContactUsMail($request));
        return response(200);
//        $
//client = new Client();
//        $request = $client->get("https://api.telegram.org/bot1058471020:AAGSMmw5d2g4PuCZiurH2Yx22N23IFI-au4/sendMessage?chat_id=@examaholic&text=".urlencode("Hello+World"));
//        $response = $request->getBody()->getContents();
//      return $response;

    }

    public function show()
    {
        return view('contact');
    }
}
