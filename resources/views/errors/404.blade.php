@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center text-black display-4" style="margin-top: 35vh">
                404 | Not Found
            </div>
        </div>
    </div>
@endsection
