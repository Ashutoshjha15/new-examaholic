@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"></div>
                    <div class="card-body text-justify">
                        <h1 class="mb-3 text-left">
                            Welcome to Examaholic
                        </h1>
                        <p>
                            <b>Examaholic</b> is a simple solution to check your <b>IGNOU results</b>. Students who have
                            enrolled in <b>IGNOU</b> can check their exam results of BCA, MCA, MP, MBP, PGDHRM, PGDFM,
                            PGDOM, PGDMM, PGDFMP. Using <b>Examaholic</b> students can not just download their <b>grade
                                card</b> but can also view their <b>term end result</b> with percentage scored.
                        </p>
                        <p>
                            IGNOU students can not only just view their grade card, but also, download their <b><a
                                    href="{{ route('hall-ticket') }}">IGNOU Hall Ticket</a></b> directly from
                            Examaholic. Yes, no more banging head against a wall by browsing links that mislead students
                            with n number of links, rather just one destination for all the material you need.
                        </p>
                        <p>
                            <b>IGNOU students</b> can now forget to go to the <b>official website of IGNOU</b> to check
                            their results and can use our system to <b>check results</b> in a simplified manner. Check
                            your all <b>semester results</b> along with total marks and backlogs. You can also check out
                            other <b>student's results</b> within no time.
                        </p>
                        <p><b>Students can check the results of the following programs:</b></p>
                        <ul>
                            <li><a href="{{route('percentage.show','BCA')}}">BCA - Bachelor of Computer Applications</a>
                            </li>
                            <li><a href="{{route('percentage.show','MCA')}}">MCA - Master of Computer Application</a>
                            </li>
                            <li><a href="{{route('percentage.show','MP')}}">MP - Management Programmes</a></li>
                            <li><a href="{{route('percentage.show','MPB')}}">MPB - Master of Business Administration
                                    (Banking & Finance)</a></li>
                            <li><a href="{{route('percentage.show','PGDHRM')}}">PGDHRM - Post Graduate Diploma in Human
                                    Resource Management</a></li>
                            <li><a href="{{route('percentage.show','PGDFM')}}">PGDFM - Post Graduate Diploma in
                                    Financial Management</a></li>
                            <li><a href="{{route('percentage.show','PGDOM')}}">PGDOM - Post Graduate Diploma in
                                    Operations Management</a></li>
                            <li><a href="{{route('percentage.show','PGDMM')}}">PGDMM - Post Graduate Diploma in
                                    Marketing Management</a></li>
                            <li><a href="{{route('percentage.show','PGDFMP')}}">PGDFMP - Post Graduate Diploma in
                                    Financial Markets Practice</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
