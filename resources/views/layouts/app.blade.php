<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <!-- Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-122463929-3"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());

        gtag('config', 'UA-122463929-3');
    </script>
    <!-- /Google Analytics -->

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name=description
          content="Examaholic -- Ignou percentage calculator to get a summary of MCA, BCA, MP, MPB, PGDHRM, PGDFM, PGDOM, PGDMM, PGDFMP.">
    <meta name=keywords
          content="IGNOU, BCA, MCA, calculator, grade card, percentage calculator, ignou, percentage calculator">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Examaholic</title>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico"/>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="d-flex flex-column min-vh-100">
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ route('index') }}">
                Examaholic
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','BCA')}}">
                            BCA
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','MCA')}}">
                            MCA
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','MP')}}">
                            MP
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','MPB')}}">
                            MPB
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','PGDHRM')}}">
                            PGDHRM
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','PGDHRM')}}">
                            PGDFM
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','PGDOM')}}">
                            PGDOM
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','PGDMM')}}">
                            PGDMM
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','PGDFMP')}}">
                            PGDFMP
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','BA')}}">
                            BA
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','BCOM')}}">
                            BCOM
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','BDP')}}">
                            BDP
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','BSC')}}">
                            BSC
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{route('percentage.show','ASSO')}}">
                            ASSO
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4 flex-grow-1">
        @yield('content')
    </main>

    <footer class="bg-white py-3 shadow-sm">
        <div class="container d-lg-flex d-md-flex d-block">
            <div class="flex-grow-1 text-lg-left text-md-left text-center">
                &copy; Copyright {{ now()->year }} All Rights Reserved.
            </div>
            <div class="flex-grow-1 text-lg-right text-md-right text-center">
                <a href="{{ route('index') }}" class="px-2">Home</a>
                <a href="{{ route('projects') }}" class="px-2">Projects</a>
                <a href="{{ route('contact') }}" class="px-2">Contact</a>
                <a href="/sitemap.xml" class="px-2">Sitemap</a>
            </div>
        </div>
    </footer>

</div>

<!-- Scripts -->
<script src="{{ asset('js/app.js') }}" defer></script>
<script src="https://use.fontawesome.com/1fe73434a9.js"></script>

<!-- Load Facebook SDK for JavaScript -->
<div id="fb-root"></div>
<script>
    window.fbAsyncInit = function () {
        FB.init({
            xfbml: true,
            version: 'v5.0'
        });
    };

    (function (d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>

<!-- Your customer chat code -->
<div class="fb-customerchat"
     attribution=setup_tool
     page_id="781591428904368"
     theme_color="#3490dc">
</div>
</body>
</html>
