@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                    </div>
                    <div class="card-body text-justify">
                        <table width="750" align="center" bgcolor="#FFFFFF">
                            <tr>
                                <td height="108" width="180">
                                    <div align="left"><img src="logo.jpg" width="94%" height="83%"> <br>
                                    </div>
                                </td>
                                <td height="108" width="558">
                                    <div align="center"><font face="Arial, Helvetica, sans-serif" size="6"><font face="Geneva, Arial, Helvetica, san-serif">Indira
                                                Gandhi National Open University</font></font><br>
                                    </div>
                                    <div align="center"><b> <font face="Arial, Helvetica, sans-serif" size="4"><em>Hall
                                                    Ticket December-2019, Term End Examination</em></font><br>

                                        </b></div>
                                </td>
                            </tr>
                            <tr>
                                <td height="275" colspan="2">

                                    <form name="FRMEno" method=post action="{{route('get-hall-ticket')}}">
                                        @csrf
                                        <font class="cfont">
                                            <div align="center">


                                                <table width="400">
                                                    <tr>
                                                        <td width="269"><font class="cfont">Enter Nine Digit Enrolment Number:</font></td>
                                                        <td width="119"><font class="cfont"><font class="cfont">
                                                                    <input type=text name=eno maxlength=9 size=20>
                                                                </font></font></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="269"><font class="cfont">Select Program: </font></td>
                                                        <td width="119"><font class="cfont">
                                                                <select name=program>
                                                                    <option value=''>--Select--</option>

                                                                    <option>ACISE</option><option>ACPDM</option><option>ASSO</option><option>BA</option><option>BARCH</option><option>BBARL</option><option>BCA</option><option>BCOM</option><option>BCOMAF</option><option>BCOMCAA</option><option>BCOMFCA</option><option>BED</option><option>BHM</option><option>BLIS</option><option>BPCCHN</option><option>BPP</option><option>BSC</option><option>BSCN</option><option>BSW</option><option>BTCM</option><option>BTME</option><option>BTS</option><option>BTWRE</option><option>CAFE</option><option>CAHT</option><option>CAL</option><option>CBS</option><option>CCITSK</option><option>CCLBL</option><option>CCP</option><option>CCPD</option><option>CCR</option><option>CDM</option><option>CES</option><option>CETM</option><option>CFAID</option><option>CFDE</option><option>CFE</option><option>CFL</option><option>CFN</option><option>CFO</option><option>CGCA</option><option>CGDA</option><option>CGL</option><option>CHBHC</option><option>CHCWM</option><option>CHHA</option><option>CHR</option><option>CIB</option><option>CIG</option><option>CIHL</option><option>CIS</option><option>CIT</option><option>CJL</option><option>CKLC</option><option>CLIS</option><option>CLTA</option><option>CMCHN</option><option>CNCC</option><option>CNIN</option><option>CNM</option><option>COF</option><option>CPABN</option><option>CPAHM</option><option>CPAKM</option><option>CPATHA</option><option>CPEL</option><option>CPF</option><option>CPHA</option><option>CPLT</option><option>CPVE</option><option>CPY</option><option>CRD</option><option>CRUL</option><option>CSLC</option><option>CSWCJS</option><option>CTE</option><option>CTPM</option><option>CTRBS</option><option>CTS</option><option>CUL</option><option>CVAA</option><option>CVAP</option><option>CWHM</option><option>DAFE</option><option>DAQ</option><option>DBPOFA</option><option>DCCN</option><option>DCE</option><option>DDT</option><option>DECE</option><option>DELED</option><option>DEVMT</option><option>DFPT</option><option>DIPP</option><option>DIR</option><option>DMT</option><option>DNA</option><option>DNHE</option><option>DPLAD</option><option>DPVCPO</option><option>DTG</option><option>DTS</option><option>DUL</option><option>DVAPFV</option><option>DWED</option><option>DWM</option><option>MAAE</option><option>MAAN</option><option>MADE</option><option>MADVS</option><option>MAEDS</option><option>MAEDU</option><option>MAGD</option><option>MAH</option><option>MAPC</option><option>MAPY</option><option>MARD</option><option>MATS</option><option>MAWGS</option><option>MBAHM</option><option>MCA</option><option>MCOM</option><option>MCOMBPCG</option><option>MCOMFT</option><option>MCOMMAFS</option><option>MEC</option><option>MED</option><option>MEG</option><option>MGPS</option><option>MHA</option><option>MHD</option><option>MLIS</option><option>MP</option><option>MPA</option><option>MPB</option><option>MPHILCHEM</option><option>MPHILJMC</option><option>MPS</option><option>MSCCFT</option><option>MSCDFSM</option><option>MSMACS</option><option>MSO</option><option>MSW</option><option>MSWC</option><option>MTM</option><option>MTTM</option><option>PGCACP</option><option>PGCAE</option><option>PGCAP</option><option>PGCBHT</option><option>PGCCC</option><option>PGCCL</option><option>PGCEDS</option><option>PGCGI</option><option>PGCGPS</option><option>PGCIATIVI</option><option>PGCPP</option><option>PGDAC</option><option>PGDAE</option><option>PGDAPP</option><option>PGDAST</option><option>PGDBP</option><option>PGDCFT</option><option>PGDCJ</option><option>PGDCOUN</option><option>PGDDM</option><option>PGDEDS</option><option>PGDEMA</option><option>PGDEOH</option><option>PGDESD</option><option>PGDET</option><option>PGDFCS</option><option>PGDFM</option><option>PGDFMP</option><option>PGDFSQM</option><option>PGDGM</option><option>PGDGPS</option><option>PGDHE</option><option>PGDHHM</option><option>PGDHIVM</option><option>PGDHO</option><option>PGDHRM</option><option>PGDIBO</option><option>PGDIPR</option><option>PGDIS</option><option>PGDLAN</option><option>PGDMCH</option><option>PGDMH</option><option>PGDMM</option><option>PGDOM</option><option>PGDPM</option><option>PGDPPED</option><option>PGDPSM</option><option>PGDRD</option><option>PGDSLM</option><option>PGDSS</option><option>PGDT</option><option>PGDUPDL</option><option>PGDWGS</option><option>PGJMC</option><option>PHDBC</option><option>PHDCHEM</option><option>PHDCOM</option><option>PHDENG</option><option>PHDES</option><option>PHDEV</option><option>PHDGDS</option><option>PHDGY</option><option>PHDJMC</option><option>PHDMGMT</option><option>PHDNS</option><option>PHDPH</option><option>PHDPVA</option><option>PHDRD</option><option>PHDSW</option><option>PHDTT</option><option>PHDWS</option>
                                                                </select>
                                                            </font></td>
                                                    </tr>
                                                    <tr>
                                                        <td width="269">&nbsp;</td>
                                                        <td width="119">&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="2" height="26">
                                                            <div align="center">
                                                                <input type=submit value=Submit name=submit onClick="return fnsubmit(this)">
                                                            </div>
                                                        </td>
                                                    </tr>

                                                </table>



                                                <font class="cfont"> </font><br>
                                            </div>
                                        </font>
                                        <div align="center">
                                            <input type=hidden value='OK' name=hidden_submit>
                                        </div>
                                    </form>

                                </td>
                            </tr>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection
