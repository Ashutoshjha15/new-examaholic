@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card mb-5">
                    <div class="card-header">We provide IT services</div>
                    <div class="card-body">
                        <p>
                            If you or someone around you are looking for a <a href="https://craftedbyfew.com/"
                                                                              target="_blank">beautiful website</a>, an
                            <a href="https://craftedbyfew.com/" target="_blank">amazing app</a>, or willing
                            to promote your business either through <a href="https://craftedbyfew.com/" target="_blank">digital
                                marketing</a> or <a href="https://craftedbyfew.com/" target="_blank">Google's local
                                business listing</a>, then you can contact us for quality work at reasonable
                            charges.
                        </p>
                        <p>
                            BCA and MCA students can also get in touch with us for their minor and major projects. The
                            projects we provide are well commented to help the students understand the overall
                            functionality of the website without wasting much time. Unlike others, <a
                                href="{{ route('index') }}">Examaholic</a> provides projects to students in a
                            student-friendly budget.
                        </p>
                    </div>
                </div>
                <contact-form subject="Project related"></contact-form>
            </div>
        </div>
    </div>

@endsection
