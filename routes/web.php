<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::resource('percentage', 'PercentageCalculator');
Route::resource('tess','TestController');
Route::get('/', 'HomeController@index')->name('index');
//Auth::routes();

Route::post('/contact/send-mail', 'ContactController@store');
Route::get('/mca-percentage/', 'PercentageCalculator@calculate')->name('mca-percentage');
Route::get('/projects', 'ProjectController@show')->name('projects');
Route::get('/contact', 'ContactController@show')->name('contact');
Route::get('/hall-ticket','HallTicketController@show')->name('hall-ticket');
Route::post('/hall-ticket', 'HallTicketController@calculate')->name('get-hall-ticket');


